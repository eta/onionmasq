package org.torproject.artitoyvpn.ui.appListing;

import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;

import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.databinding.AppListingFragmentBinding;
import org.torproject.artitoyvpn.vpn.VpnStatusObservable;

public class AppListingFragment extends Fragment {
    private AppListingViewModel mViewModel;
    private AppListingFragmentBinding mBinding;
    private AppListAdapter appListAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mBinding = AppListingFragmentBinding.inflate(inflater, container, false);
        mViewModel = new ViewModelProvider(this).get(AppListingViewModel.class);


        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        mBinding.appList.setLayoutManager(layoutManager);
        appListAdapter = new AppListAdapter(mViewModel);
        mBinding.appList.setAdapter(appListAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(requireContext(), layoutManager.getOrientation());
        mBinding.appList.addItemDecoration(dividerItemDecoration);


        mViewModel.getList().observe(getViewLifecycleOwner(), appListAdapter::setData);
        mViewModel.isLoading().observe(getViewLifecycleOwner(), this::toggleProgressBar);
        mViewModel.getAppCount().observe(getViewLifecycleOwner(), this::setAppCount);
        mViewModel.getItemChanged().observe(getViewLifecycleOwner(), this::setItemChange);
        mViewModel.getRestartVpnHeadsUp().observe(getViewLifecycleOwner(), (unused) -> showVpnRestartSnack());
        VpnStatusObservable.getStatus().observe(getViewLifecycleOwner(), status -> mViewModel.setVpnStatus(status));


        return mBinding.getRoot();
    }

    private void toggleProgressBar(boolean isLoading) {
        mBinding.progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    private void setAppCount(Pair<Integer, Integer> totalAndSelected) {
        mBinding.textView2.setText(getString(R.string.info_app_count, String.valueOf(totalAndSelected.first), String.valueOf(totalAndSelected.second)));
    }

    private void setItemChange(int pos) {
        appListAdapter.notifyItemChanged(pos);
    }

    private void showVpnRestartSnack() {
        Snackbar.make(mBinding.appListingRoot, "Restart VPN to make new selection effective", Snackbar.LENGTH_LONG).show();
    }
}