package org.torproject;

public class OnionMasq {
    public static native int runProxy(int filedescriptor, String interfaceName, String cacheDir);
    public static native int initLogging();

    static {
        System.loadLibrary("onionmasq_mobile");
    }
}
