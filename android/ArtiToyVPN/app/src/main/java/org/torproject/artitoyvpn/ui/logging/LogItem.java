package org.torproject.artitoyvpn.ui.logging;

public class LogItem {
    public final String content;
    public final String timestamp;

    public LogItem(String timestamp, String content) {
        this.timestamp = timestamp;
        this.content = content.trim();
    }

    @Override
    public String toString() {
        return timestamp + " " + content;
    }

    public String toString(boolean showTimestamp) {
        String result = "";
        result += showTimestamp ? timestamp + " " : "";
        result += content;
        return result;
    }
}
