// Copyright (c) 2021, The Tor Project, Inc.
// See LICENSE for licensing information.

use core::cell::RefCell;
use std::collections::VecDeque;

use smoltcp::phy::{Device, DeviceCapabilities, RxToken, TxToken};
use smoltcp::time::Instant;

pub struct VirtualDeviceState {
    incoming_packets: VecDeque<Vec<u8>>,
    poll_mode: VirtualDevicePollMode,
}

impl VirtualDeviceState {
    pub fn new() -> VirtualDeviceState {
        VirtualDeviceState {
            incoming_packets: VecDeque::new(),
            poll_mode: VirtualDevicePollMode::QueuePackets,
        }
    }

    pub fn set_poll_mode(&mut self, poll_mode: VirtualDevicePollMode) {
        self.poll_mode = poll_mode;
    }

    pub fn poll_mode(&self) -> VirtualDevicePollMode {
        self.poll_mode
    }

    pub fn push_packet(&mut self, packet: Vec<u8>) {
        self.incoming_packets.push_back(packet);
    }

    pub fn pop_packet(&mut self) -> Option<Vec<u8>> {
        self.incoming_packets.pop_front()
    }
}

#[derive(Copy, Clone)]
pub enum VirtualDevicePollMode {
    QueuePackets,
    ProcessQueuedPackets,
}

pub struct VirtualDevice<D>
where
    D: for<'a> Device<'a>,
{
    device: D,
    state: RefCell<VirtualDeviceState>,
}

impl<'b, D: for<'a> Device<'a>> VirtualDevice<D> {
    pub fn new(device: D) -> VirtualDevice<D> {
        VirtualDevice {
            device,
            state: RefCell::new(VirtualDeviceState::new()),
        }
    }

    pub fn incoming_packets(&self) -> VecDeque<Vec<u8>> {
        self.state.borrow().incoming_packets.clone()
    }

    pub fn set_poll_mode(&self, poll_mode: VirtualDevicePollMode) {
        self.state.borrow_mut().set_poll_mode(poll_mode);
    }

    pub fn poll_mode(&self) -> VirtualDevicePollMode {
        self.state.borrow().poll_mode()
    }

    fn receive_and_queue_packets(&mut self) {
        // Keep track of how many packets we queued.
        let mut packet_counter = 0;

        // We receive packets from our internal device and discard the Tx-token since we can
        // regenerate that from the transmit() method later.
        while let Some((rx_token, _tx_token)) = self.device.receive() {
            // We consume the Rx-token to extract the packet content for inspection. The call to
            // unwrap() should be safe here since we return Ok(...) explicitly.
            let rx_packet = rx_token
                .consume(Instant::now(), |packet| Ok(Vec::from(packet)))
                .unwrap();

            // Append the packet to our queue.
            self.state.borrow_mut().push_packet(rx_packet);

            // Increment our packet counter.
            packet_counter += 1;
        }

        // The first loop iteration is always zero.
        if packet_counter > 0 {
            log::trace!("Queued {} packet(s) from the Tun interface", packet_counter);
        }
    }

    fn process_one_queued_packet(
        &'b mut self,
    ) -> Option<(
        <VirtualDevice<D> as Device<'b>>::RxToken,
        <VirtualDevice<D> as Device<'b>>::TxToken,
    )> {
        self.state.borrow_mut().pop_packet().map(|packet| {
            log::trace!("Processing queued packet from Tun interface");

            // We create a Tx token from our parent device's transmit() method. On TunTapInterface
            // this should never fail and thus the unwrap() call should be safe.
            let device_tx_token = self.device.transmit().unwrap();

            // Create our virtual Rx and Tx tokens.
            let rx_token = VirtualRxToken::new(&self.state, packet);
            let tx_token = VirtualTxToken::new(&self.state, device_tx_token);

            // Return values.
            (rx_token, tx_token)
        })
    }
}

pub struct VirtualRxToken<'a> {
    state: &'a RefCell<VirtualDeviceState>,
    packet: Vec<u8>,
}

impl<'a> VirtualRxToken<'a> {
    pub fn new(state: &'a RefCell<VirtualDeviceState>, packet: Vec<u8>) -> VirtualRxToken<'a> {
        VirtualRxToken { state, packet }
    }
}

impl<'a> RxToken for VirtualRxToken<'a> {
    fn consume<R, F: FnOnce(&mut [u8]) -> smoltcp::Result<R>>(
        mut self,
        _timestamp: Instant,
        f: F,
    ) -> smoltcp::Result<R> {
        f(&mut self.packet[..])
    }
}

pub struct VirtualTxToken<'a, Tx: TxToken> {
    state: &'a RefCell<VirtualDeviceState>,
    token: Tx,
}

impl<'a, Tx: TxToken> VirtualTxToken<'a, Tx> {
    pub fn new(state: &'a RefCell<VirtualDeviceState>, token: Tx) -> VirtualTxToken<'a, Tx> {
        VirtualTxToken { state, token }
    }
}

impl<'a, Tx: TxToken> TxToken for VirtualTxToken<'a, Tx> {
    fn consume<R, F: FnOnce(&mut [u8]) -> smoltcp::Result<R>>(
        self,
        timestamp: Instant,
        length: usize,
        f: F,
    ) -> smoltcp::Result<R> {
        self.token.consume(timestamp, length, f)
    }
}

impl<'a, D> Device<'a> for VirtualDevice<D>
where
    D: for<'b> Device<'b>,
{
    type RxToken = VirtualRxToken<'a>;
    type TxToken = VirtualTxToken<'a, <D as Device<'a>>::TxToken>;

    fn capabilities(&self) -> DeviceCapabilities {
        self.device.capabilities()
    }

    fn receive(&'a mut self) -> Option<(Self::RxToken, Self::TxToken)> {
        match self.poll_mode() {
            VirtualDevicePollMode::QueuePackets => {
                self.receive_and_queue_packets();

                // Always return None here.
                None
            }
            VirtualDevicePollMode::ProcessQueuedPackets => self.process_one_queued_packet(),
        }
    }

    fn transmit(&'a mut self) -> Option<Self::TxToken> {
        self.device
            .transmit()
            .map(|token| VirtualTxToken::new(&self.state, token))
    }
}
