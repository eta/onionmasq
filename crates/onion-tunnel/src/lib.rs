mod device;
mod dns;
mod netlink;
mod parser;
mod proxy;
mod runtime;
mod socket;

use arti_client::{TorClient, TorClientConfig};
use device::VirtualDevice;
use dns::{DnsManager, LockedDnsCookies};
use log::{debug, info};
use proxy::ArtiProxy;
use runtime::OnionTunnelArtiRuntime;
use smoltcp::{
    iface::{Interface, InterfaceBuilder, Routes},
    phy::{Medium, TunTapInterface},
    wire::{IpAddress, IpCidr, Ipv4Address},
};
use std::{
    collections::BTreeMap,
    io::{Error, ErrorKind},
    sync::{Arc, Mutex},
};

use crate::{parser::Parser, socket::TcpSocket};

pub struct OnionTunnel {
    iface_fd: i32,
    iface_name: String,
    iface: Arc<Mutex<Interface<'static, VirtualDevice<TunTapInterface>>>>,
    arti: TorClient<OnionTunnelArtiRuntime>,
    dns_cookies: Option<LockedDnsCookies>,
}

impl OnionTunnel {
    pub async fn create_with_fd(iface_name: &str, fd: i32) -> Result<Self, Error> {
        debug!("OnionTunnel create_with_fd {}", &iface_name);
        let tun: TunTapInterface;
        match TunTapInterface::new_with_fd(fd, Medium::Ip) {
            Ok(v) => {
                debug!("successfully created tun interface");
                tun = v;
            }
            Err(_e) => {
                debug!("couldn't create tun interface");
                return Err(Error::new(
                    ErrorKind::PermissionDenied,
                    "couldn't create tun interface",
                ));
            }
        }

        let device = VirtualDevice::new(tun);

        let config = TorClientConfig::default();
        let rt = OnionTunnelArtiRuntime::new().await;
        let arti_builder = TorClient::with_runtime(rt).config(config);
        let arti = arti_builder.create_unbootstrapped().unwrap();

        // routes should already be configured
        let iface_builder = InterfaceBuilder::new(device, Vec::new());
        let iface = iface_builder.finalize();
        let iface_fd = iface.device().as_raw_fd();
        let iface = Arc::new(Mutex::new(iface));

        Ok(Self {
            arti,
            iface_fd,
            iface_name: iface_name.to_string(),
            iface,
            dns_cookies: None,
        })
    }

    pub async fn new(iface_name: &str) -> Self {
        let tun = TunTapInterface::new(iface_name, Medium::Ip).expect("Unable to create TUN iface");
        let device = VirtualDevice::new(tun);

        // Create our iface.
        let mut routes = Routes::new(BTreeMap::new());
        routes
            .add_default_ipv4_route(Ipv4Address::new(0, 0, 0, 1))
            .expect("Unable to add default IPv4 route");
        let iface_builder = InterfaceBuilder::new(device, Vec::new())
            .ip_addrs([IpCidr::new(IpAddress::v4(0, 0, 0, 1), 0)])
            .routes(routes)
            .any_ip(true);

        let config = TorClientConfig::default();
        let rt = OnionTunnelArtiRuntime::new().await;
        let arti_builder = TorClient::with_runtime(rt).config(config);
        let arti = arti_builder.create_unbootstrapped().unwrap();

        let iface = iface_builder.finalize();
        let iface_fd = iface.device().as_raw_fd();
        let iface = Arc::new(Mutex::new(iface));

        Self {
            arti,
            iface_fd,
            iface_name: iface_name.to_string(),
            iface,
            dns_cookies: None,
        }
    }

    fn iface_poll(&self, timestamp: smoltcp::time::Instant) {
        match self.iface.lock().unwrap().poll(timestamp) {
            Ok(_) | Err(smoltcp::Error::Exhausted) => {}
            Err(error) => {
                debug!("Poll error: {}", error)
            }
        }
    }

    fn proxy(&mut self, socket: TcpSocket) {
        let mut proxy = ArtiProxy::new(
            socket,
            self.arti.clone(),
            self.dns_cookies.as_ref().unwrap().clone(),
        );
        tokio::spawn(async move { proxy.start().await });
    }

    fn dns(&mut self) {
        // Spawn the DNS manager service.
        let mut dns_manager = DnsManager::new(self.iface.clone());
        self.dns_cookies = Some(dns_manager.cookies());
        tokio::spawn(async move { dns_manager.start().await });
    }

    pub async fn start(&mut self) {
        info!("Starting onion tunnel on TUN iface {}", self.iface_name);

        // Spawn the DNS manager service.
        self.dns();

        loop {
            let timestamp = smoltcp::time::Instant::now();

            // The first poll we queue packets in the receive queue.
            self.iface_poll(timestamp);

            let mut packets = self.iface.lock().unwrap().device_mut().take_recv_queue();

            // Handle incoming packet. Drain packets as we process them.
            while let Some(packet) = packets.pop_front() {
                if let Some(tcp_socket) = Parser::parse(packet.clone()).take() {
                    let socket = TcpSocket::new(self.iface.clone(), tcp_socket);
                    self.proxy(socket);
                }
                self.iface.lock().unwrap().device_mut().put_packet(packet);
            }

            // The second poll we do process packets in the receive queue from the first poll.
            self.iface_poll(timestamp);

            let timeout = self.iface.lock().unwrap().poll_delay(timestamp);
            smoltcp::phy::wait(self.iface_fd, timeout).expect("wait error");
        }
    }
}
