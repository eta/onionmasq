use std::future::Future;
use std::io::Result as IoResult;
use std::net::SocketAddr;
use std::pin::Pin;
use std::task::{Context, Poll};

use futures::{AsyncRead, AsyncWrite, FutureExt, Stream};
use tor_rtcompat::tokio::TokioNativeTlsRuntime as ArtiRuntime;
use tor_rtcompat::{CompoundRuntime, Runtime, TcpListener, TcpProvider};

use crate::netlink::Netlink;

pub type RuntimeInner = CompoundRuntime<
    ArtiRuntime,
    ArtiRuntime,
    OnionTunnelArtiTcpProvider<ArtiRuntime>,
    ArtiRuntime,
    ArtiRuntime,
>;

#[derive(Clone)]
pub struct OnionTunnelArtiRuntime {
    inner: RuntimeInner,
}

impl OnionTunnelArtiRuntime {
    pub async fn new() -> Self {
        let rt = ArtiRuntime::current().expect("Unable to get runtime");
        let tcp_provider = OnionTunnelArtiTcpProvider {
            netlink: Netlink::new().await,
            inner: rt.clone(),
        };
        Self {
            inner: CompoundRuntime::new(rt.clone(), rt.clone(), tcp_provider, rt.clone(), rt),
        }
    }
}

impl futures::task::Spawn for OnionTunnelArtiRuntime {
    fn spawn_obj(
        &self,
        future: futures::future::FutureObj<'static, ()>,
    ) -> Result<(), futures::task::SpawnError> {
        self.inner.spawn_obj(future)
    }
}

impl tor_rtcompat::BlockOn for OnionTunnelArtiRuntime {
    #[inline]
    fn block_on<F: Future>(&self, future: F) -> F::Output {
        self.inner.block_on(future)
    }
}

impl tor_rtcompat::SleepProvider for OnionTunnelArtiRuntime {
    type SleepFuture = <RuntimeInner as tor_rtcompat::SleepProvider>::SleepFuture;
    #[inline]
    fn sleep(&self, duration: std::time::Duration) -> Self::SleepFuture {
        self.inner.sleep(duration)
    }
}

#[async_trait::async_trait]
impl tor_rtcompat::TcpProvider for OnionTunnelArtiRuntime {
    type TcpStream = <RuntimeInner as tor_rtcompat::TcpProvider>::TcpStream;
    type TcpListener = <RuntimeInner as tor_rtcompat::TcpProvider>::TcpListener;

    #[inline]
    async fn connect(&self, addr: &std::net::SocketAddr) -> std::io::Result<Self::TcpStream> {
        self.inner.connect(addr).await
    }

    #[inline]
    async fn listen(&self, addr: &std::net::SocketAddr) -> std::io::Result<Self::TcpListener> {
        self.inner.listen(addr).await
    }
}

impl<S> tor_rtcompat::TlsProvider<S> for OnionTunnelArtiRuntime
where
    S: futures::AsyncRead + futures::AsyncWrite + Unpin + Send + 'static,
{
    type Connector = <RuntimeInner as tor_rtcompat::TlsProvider<S>>::Connector;
    type TlsStream = <RuntimeInner as tor_rtcompat::TlsProvider<S>>::TlsStream;

    #[inline]
    fn tls_connector(&self) -> Self::Connector {
        self.inner.tls_connector()
    }
}

#[async_trait::async_trait]
impl tor_rtcompat::UdpProvider for OnionTunnelArtiRuntime {
    type UdpSocket = <RuntimeInner as tor_rtcompat::UdpProvider>::UdpSocket;

    #[inline]
    async fn bind(&self, addr: &SocketAddr) -> IoResult<Self::UdpSocket> {
        self.inner.bind(addr).await
    }
}

pub struct OnionTunnelArtiTcpProvider<T> {
    netlink: Netlink,
    // Underlying TCP provider.
    inner: T,
}

pub struct OnionTunnelTcpStream<T> {
    // Underlying TCP stream.
    inner: T,
}

/// A wrapper over a `TcpListener`.
pub struct OnionTunnelTcpListener<T> {
    inner: T,
}

/// An `Incoming` type for our `CustomTcpListener`.
pub struct OnionTunnelIncoming<T> {
    inner: T,
}

#[async_trait::async_trait]
impl<T: Runtime> TcpProvider for OnionTunnelArtiTcpProvider<T> {
    type TcpStream = OnionTunnelTcpStream<T::TcpStream>;
    type TcpListener = OnionTunnelTcpListener<T::TcpListener>;

    async fn connect(&self, addr: &SocketAddr) -> IoResult<Self::TcpStream> {
        // Use the underlying TCP provider implementation to do the connection, and
        // return our wrapper around it once done.
        println!("tcp connect to {}", addr);
        let _ = self.netlink.add_passthrough_route(&addr.ip().into()).await;
        self.inner
            .connect(addr)
            .map(move |r| r.map(|stream| OnionTunnelTcpStream { inner: stream }))
            .boxed()
            .await
    }

    // This is also an async trait method (see above).
    async fn listen(&self, addr: &SocketAddr) -> IoResult<Self::TcpListener> {
        // Use the underlying TCP provider implementation to make the listener, and
        // return our wrapper around it once done.
        println!("tcp listen on {}", addr);
        self.inner
            .listen(addr)
            .map(|l| l.map(|listener| OnionTunnelTcpListener { inner: listener }))
            .boxed()
            .await
    }
}

// We implement `AsyncRead` and `AsyncWrite` for our custom TCP stream object.
// This implementation mostly uses the underlying stream's methods, but we insert some
// code to check for a zero-byte read (indicating stream closure), and callers closing the
// stream, and use that to update our `TcpState`.
// When we detect that the stream is closed, we run some code (in this case, just a `println!`).
impl<T> AsyncRead for OnionTunnelTcpStream<T>
where
    T: AsyncRead + Unpin,
{
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<IoResult<usize>> {
        // Call the underlying stream's method.
        Pin::new(&mut self.inner).poll_read(cx, buf)
    }

    // Do the same thing, but for `poll_read_vectored`.
    fn poll_read_vectored(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        bufs: &mut [std::io::IoSliceMut<'_>],
    ) -> Poll<IoResult<usize>> {
        Pin::new(&mut self.inner).poll_read_vectored(cx, bufs)
    }
}

// The only thing that's custom here is checking for closure. Everything else is just calling
// `self.inner`.
impl<T> AsyncWrite for OnionTunnelTcpStream<T>
where
    T: AsyncWrite + Unpin,
{
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<IoResult<usize>> {
        Pin::new(&mut self.inner).poll_write(cx, buf)
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<IoResult<()>> {
        Pin::new(&mut self.inner).poll_flush(cx)
    }

    fn poll_close(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<IoResult<()>> {
        Pin::new(&mut self.inner).poll_close(cx)
    }

    fn poll_write_vectored(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        bufs: &[std::io::IoSlice<'_>],
    ) -> Poll<IoResult<usize>> {
        Pin::new(&mut self.inner).poll_write_vectored(cx, bufs)
    }
}

type AcceptResult<T> = IoResult<(T, SocketAddr)>;

impl<T> TcpListener for OnionTunnelTcpListener<T>
where
    T: TcpListener,
{
    type TcpStream = OnionTunnelTcpStream<T::TcpStream>;
    type Incoming = OnionTunnelIncoming<T::Incoming>;

    // This is also an async trait method (see earlier commentary).
    fn accept<'a, 'b>(
        &'a self,
    ) -> Pin<Box<dyn Future<Output = AcceptResult<Self::TcpStream>> + Send + 'b>>
    where
        'a: 'b,
        Self: 'b,
    {
        // As with other implementations, we just defer to `self.inner` and wrap the result.
        self.inner
            .accept()
            .inspect(|r| {
                if let Ok((_, addr)) = r {
                    println!("accepted connection from {}", addr)
                }
            })
            .map(|r| r.map(|(stream, addr)| (OnionTunnelTcpStream { inner: stream }, addr)))
            .boxed()
    }

    fn incoming(self) -> Self::Incoming {
        OnionTunnelIncoming {
            inner: self.inner.incoming(),
        }
    }

    fn local_addr(&self) -> IoResult<SocketAddr> {
        self.inner.local_addr()
    }
}

impl<T, S> Stream for OnionTunnelIncoming<T>
where
    T: Stream<Item = IoResult<(S, SocketAddr)>> + Unpin,
{
    type Item = IoResult<(OnionTunnelTcpStream<S>, SocketAddr)>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        match Pin::new(&mut self.inner).poll_next(cx) {
            Poll::Ready(Some(Ok((stream, addr)))) => {
                Poll::Ready(Some(Ok((OnionTunnelTcpStream { inner: stream }, addr))))
            }
            Poll::Ready(Some(Err(e))) => Poll::Ready(Some(Err(e))),
            Poll::Ready(None) => Poll::Ready(None),
            Poll::Pending => Poll::Pending,
        }
    }
}
