#!/bin/bash

# Creating the jni is not implemented yet, this script just cross-compiles arti for now

# If you want to speed up compilation, you can run this script with your
# architecture as an argument:
#
#     ./ndk-make.sh arm64-v8a
#
# Possible values are armeabi-v7a, arm64-v8a, x86 and x86_64.
# You should be able to find out your architecture by running:
#
#     adb shell uname -m
#
# Or you just guess your phone's architecture to be arm64-v8a and if you
# guessed wrongly, a warning message will pop up inside the app, telling
# you what the correct one is.
#
# The values in the following lines mean the same:
#
# armeabi-v7a, armv7 and arm
# arm64-v8a, aarch64 and arm64
# x86 and i686
# (there are no synonyms for x86_64)
#
#

set -e
echo "starting time: `date`"

EXPECTED_NDK_VERSION="20.1.5948944"

: "${ANDROID_NDK_ROOT:=$ANDROID_NDK_HOME}"
: "${ANDROID_NDK_ROOT:=$ANDROID_NDK}"
if test -z "$ANDROID_NDK_ROOT"; then
    echo "Please set \$ANDROID_NDK environment variable"
    exit -1
fi

if [[ ! "${ANDROID_NDK_ROOT##*/}" == $EXPECTED_NDK_VERSION ]]; then
    echo "Error: Please set ANDROID_NDK to the NDK file path of version $EXPECTED_NDK_VERSION"
    exit 1
fi

echo Setting CARGO_TARGET environment variables.

if test -z "$NDK_HOST_TAG"; then
    KERNEL="$(uname -s | tr '[:upper:]' '[:lower:]')"
    ARCH="$(uname -m)"
    NDK_HOST_TAG="$KERNEL-$ARCH"
    echo $ARCH
    echo $KERNEL
    if [[ ${KERNEL} == "darwin" ]]; then
        TOOLCHAIN_HOST="apple-darwin"
        DATE_SPECIFIER=""
    elif [[ ${KERNEL} == "linux" ]]; then
        TOOLCHAIN_HOST=$KERNEL-gnu
        DATE_SPECIFIER="unknown-"
    else
        echo "TODO: adapt toolchain host variable for Windows"
    fi
fi

TOOLCHAIN="$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/$NDK_HOST_TAG"
ROOT_DIR=$PWD
BUILD_DIR=$ROOT_DIR/build
CARGO_TARGET_DIRECTORY=$ROOT_DIR/target
CARGO_TOOLCHAIN=+stable-$ARCH-${DATE_SPECIFIER}${TOOLCHAIN_HOST}

LLVM_PREBUILD=$TOOLCHAIN/bin
export PATH=$LLVM_PREBUILD:$PATH
export CFLAGS=-D__ANDROID_API__=21
export CARGO_TARGET_ARMV7_LINUX_ANDROIDEABI_LINKER="$TOOLCHAIN/bin/armv7a-linux-androideabi21-clang"
export CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER="$TOOLCHAIN/bin/aarch64-linux-android21-clang"
export CARGO_TARGET_I686_LINUX_ANDROID_LINKER="$TOOLCHAIN/bin/i686-linux-android21-clang"
export CARGO_TARGET_X86_64_LINUX_ANDROID_LINKER="$TOOLCHAIN/bin/x86_64-linux-android21-clang"

# Check if the argument is a correct architecture:
if test $1 && echo "armeabi-v7a arm64-v8a x86 x86_64" | grep -vwq $1; then
    echo "Architecture '$1' not known, possible values are armeabi-v7a, arm64-v8a, x86 and x86_64."
    exit
fi

# fix build on MacOS Catalina
unset CPATH

if test -z $1; then
  echo Full build

  # According to 1.45.0 changelog in https://github.com/rust-lang/rust/blob/master/RELEASES.md,
  # "The recommended way to control LTO is with Cargo profiles, either in Cargo.toml or .cargo/config, or by setting CARGO_PROFILE_<name>_LTO in the environment."
  export CARGO_PROFILE_RELEASE_LTO=on
  RELEASE="release"
  RELEASEFLAG="--release"
else
  echo Fast, partial, slow debug build. DO NOT UPLOAD THE APK ANYWHERE.

  RELEASE="debug"
  RELEASEFLAG=
fi

if test -z $1 || test $1 = armeabi-v7a; then
    echo "-- cross compiling to armv7-linux-androideabi (arm) --"
    TARGET_CC=armv7a-linux-androideabi21-clang \
    OPENSSL_INCLUDE_DIR=$ROOT_DIR/external/openssl/out/inc/armeabi-v7a \
    OPENSSL_LIB_DIR=$ROOT_DIR/external/openssl/out/lib/armeabi-v7a \
    CARGO_TARGET_DIR=$CARGO_TARGET_DIRECTORY \
    cargo $CARGO_TOOLCHAIN build $RELEASEFLAG --target armv7-linux-androideabi --lib
    if [[ ! -f ${BUILD_DIR}/armeabi-v7a ]]; then mkdir -p $BUILD_DIR/armeabi-v7a; fi
    cp $CARGO_TARGET_DIRECTORY/armv7-linux-androideabi/$RELEASE/libonionmasq_mobile.so $BUILD_DIR/armeabi-v7a
fi

if test -z $1 || test $1 = arm64-v8a; then
    echo "-- cross compiling to aarch64-linux-android (arm64) --"
    TARGET_CC=aarch64-linux-android21-clang \
    OPENSSL_INCLUDE_DIR=$ROOT_DIR/external/openssl/out/inc/arm64-v8a \
    OPENSSL_LIB_DIR=$ROOT_DIR/external/openssl/out/lib/arm64-v8a \
    CARGO_TARGET_DIR=$CARGO_TARGET_DIRECTORY \
    cargo $CARGO_TOOLCHAIN build $RELEASEFLAG --target aarch64-linux-android --lib
    if [[ ! -f ${BUILD_DIR}/arm64-v8a ]]; then mkdir -p $BUILD_DIR/arm64-v8a; fi
    cp $CARGO_TARGET_DIRECTORY/aarch64-linux-android/$RELEASE/libonionmasq_mobile.so $BUILD_DIR/arm64-v8a
fi

if test -z $1 || test $1 = x86; then
    echo "-- cross compiling to i686-linux-android (x86) --"
    TARGET_CC=i686-linux-android21-clang \
    OPENSSL_INCLUDE_DIR=$ROOT_DIR/external/openssl/out/inc/x86 \
    OPENSSL_LIB_DIR=$ROOT_DIR/external/openssl/out/lib/x86 \
    CARGO_TARGET_DIR=$CARGO_TARGET_DIRECTORY \
    cargo $CARGO_TOOLCHAIN build $RELEASEFLAG --target i686-linux-android --lib
    if [[ ! -f ${BUILD_DIR}/x86 ]]; then mkdir -p $BUILD_DIR/x86; fi
    cp $CARGO_TARGET_DIRECTORY/i686-linux-android/$RELEASE/libonionmasq_mobile.so $BUILD_DIR/x86
fi

if test -z $1 || test $1 = x86_64; then
    echo "-- cross compiling to x86_64-linux-android (x86_64) --"
    TARGET_CC=x86_64-linux-android21-clang \
    OPENSSL_INCLUDE_DIR=$ROOT_DIR/external/openssl/out/inc/x86_64 \
    OPENSSL_LIB_DIR=$ROOT_DIR/external/openssl/out/lib/x86_64 \
    CARGO_TARGET_DIR=$CARGO_TARGET_DIRECTORY \
    cargo $CARGO_TOOLCHAIN build $RELEASEFLAG --target x86_64-linux-android --lib
    if [[ ! -f ${BUILD_DIR}/x86_64 ]]; then mkdir -p $BUILD_DIR/x86_64; fi
    cp $CARGO_TARGET_DIRECTORY/x86_64-linux-android/$RELEASE/libonionmasq_mobile.so $BUILD_DIR/x86_64
fi

echo TODO: -- ndk-build --

#cd ..
# Set the right arch in Application.mk:
# oldDotMk="$(cat Application.mk)"

#if test $1; then
    # Using temporary file because `sed -i` is not portable
#    TMP=$(mktemp)
#    sed "s/APP_ABI.*/APP_ABI := $1/g" Application.mk >"$TMP"
#    mv "$TMP" Application.mk
#else
    # We are compiling for all architectures:
#    TMP=$(mktemp)
#    sed "s/APP_ABI.*/APP_ABI := armeabi-v7a arm64-v8a x86 x86_64/g" Application.mk >"$TMP"
#    mv "$TMP" Application.mk
#fi

#cd ..
# ndk-build

# cd jni
# Restore old Application.mk:
# echo "$oldDotMk" > Application.mk

# if test $1; then
#    echo "NDK_ARCH=$1" > ../ndkArch
# else
#    rm ../ndkArch 2>/dev/null || true # Remove ndkArch, ignore if it doesn't exist
# fi

# echo "ending time: `date`"
